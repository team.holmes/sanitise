const DEFAULT_REPLACEMENT = '******';

const isObject = obj => Object.prototype.toString.call(obj) === '[object Object]';

const isArrayOfObjects = obj => Array.isArray(obj) && obj.length && obj.every(x => isObject(x));

const replaceValueIfMatchedKey = (blacklist, obj, key) => {
  let cloneObj = { ...obj };
  let matchedProperty = blacklist.find(x => x.key === key);
  if (matchedProperty) {
    cloneObj[key] = matchedProperty.replaceWith || DEFAULT_REPLACEMENT
  }
  return cloneObj;
};

const sanitise = blacklist => obj => {

  if (!isArrayOfObjects(blacklist)) {
    throw new Error('Blacklist supplied must be an array of objects.');
  }

  if (!isObject(obj)) {
    throw new Error('Object supplied to sanitise is not of type object.');
  }

  let cloneObj = { ...obj };

  for (let key in cloneObj) {
    if (isObject(cloneObj[key])) {
      cloneObj[key] = sanitise(blacklist)(cloneObj[key]);
    } else {
      cloneObj = replaceValueIfMatchedKey(blacklist, cloneObj, key)
    }
  }

  return cloneObj;
}

module.exports = sanitise;