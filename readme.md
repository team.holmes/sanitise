# Sanitise
A simple utility function that sanitises specified object key values.

## Why this exists
I was becoming quite sick of having to always think about sanitising sensitive data to logs so i wrote this little utility to take the headache out of it.

## Key Concepts
Require in the module passing in an array of object(s) which detail the key and the replacement value to use (e.g. ```[{key: 'secretkey': replaceWith: 'xxxxx'}]``` ). This will return a function which takes the object you wish to sanitise, a new sanitised object is then returned.

## Example
```javascript
const sanitise = require('sanitise');

const PROPERTIES_TO_SANITISE = [
    { key: 'name', replaceWith: '========' },
    { key: 'postcode', replaceWith: '--------' },
    { key: 'age' } // default replacement of "******" will be used
];

const sanitiseData = sanitise(PROPERTIES_TO_SANITISE);

const TEST_DATA = {
    name: '[this will be replaced]',
    age: '[this will be replaced]',
    nationality: 'british',
    address: {
      street1: '47 somewhere',
      postcode: '[this will be replaced]'
    }
  };

let sanitisedObject = sanitiseData(TEST_DATA)

```

## Want to help?
Feel free to get involved and increase the value of this little library, simply pop in a branch and PR and we can take if from there.