const assert = require('chai').assert;

const subject = require('../src/index');

describe('sanitise', () => {
  const PROPERTIES_TO_SANITISE = [
    { key: 'name', replaceWith: '========' },
    { key: 'postcode', replaceWith: '--------' }
  ];

  const TEST_DATA = {
    name: 'REPLACED',
    age: 47,
    nationality: 'british',
    address: {
      street: '47 somewhere',
      postcode: 'somepostcode'
    }
  };

  it('replaces correct property values', () => {
    let sanitised = subject([{key: 'name'}])(TEST_DATA);
    assert.deepEqual(sanitised, {
      name: '******',
      age: 47,
      nationality: 'british',
      address: {
        street: '47 somewhere',
        postcode: 'somepostcode'
      }
    });
  });

  it('uses default replacement if none provided', () => {
    let sanitised = subject(PROPERTIES_TO_SANITISE)(TEST_DATA);
    assert.deepEqual(sanitised, {
      name: '========',
      age: 47,
      nationality: 'british',
      address: {
        street: '47 somewhere',
        postcode: '--------'
      }
    });
  });

  it('throws an error when blacklist is not supplied as type object', () => {
    assert.throws(subject(), Error, 'Blacklist supplied must be an array of objects.');
    assert.throws(subject('asda'), Error, 'Blacklist supplied must be an array of objects.');
    assert.throws(subject(673), Error, 'Blacklist supplied must be an array of objects.');
    assert.throws(subject('asda'), Error, 'Blacklist supplied must be an array of objects.');
    assert.throws(subject([]), Error, 'Blacklist supplied must be an array of objects.');
    assert.throws(subject({foo: 'bar'}), Error, 'Blacklist supplied must be an array of objects.');
  });

  it('throws an error when the item to sanitise is not of type object', () => {
    let sanitised = subject(PROPERTIES_TO_SANITISE);
    assert.throws(sanitised, Error, 'Object supplied to sanitise is not of type object.');
  });

})